
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';
import 'package:messenger/screens/login_screen.dart';
import 'package:messenger/screens/registration_screen.dart';

class LocalNotificationService {
  static final _notificationsPlugin = FlutterLocalNotificationsPlugin();

  static void initialize(BuildContext context) {
    const initializationSettings = InitializationSettings(
        android: AndroidInitializationSettings("@mipmap/ic_launcher"));
    _notificationsPlugin.initialize(initializationSettings,
        onSelectNotification: (payload) async {
          if (payload == "rakib") {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return LoginScreen(message: payload,);
            }));
          } else if (payload == "fahim") {
            Navigator.push(context, MaterialPageRoute(builder: (context) {
              return RegistrationScreen();
            }));
          } else {
            print(payload);
          }
        });
  }

  //ok

  static void display(RemoteMessage message) {
    final id = DateTime.now().millisecondsSinceEpoch ~/ 1000;

    const NotificationDetails notificationDetails = NotificationDetails(
        android: AndroidNotificationDetails(
            "com.pondit.messenger", "notification-channel",
            importance: Importance.max, priority: Priority.high));

    _notificationsPlugin.show(id, message.notification!.title,
        message.notification!.body, notificationDetails,
        payload: message.data["name"]);
  }
}
