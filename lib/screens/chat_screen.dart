import 'dart:io';

import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:flutter/material.dart';
import 'package:image_picker/image_picker.dart';
import 'package:messenger/constants.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

final firestore = FirebaseFirestore.instance;
User? loggedInUser;

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  FirebaseAuth? auth;
  TextEditingController? textEditingController;

  @override
  void initState() {
    super.initState();
    textEditingController = TextEditingController();
    auth = FirebaseAuth.instance;
    getCurrentUser();
  }

  void getCurrentUser() {
    try {
      loggedInUser = auth!.currentUser;
    } catch (error) {
      Alert(
        context: context,
        type: AlertType.error,
        title: "Signup Failed",
        desc: error.toString(),
      ).show();
    }
  }

  void saveMessage(String imageUrl) {
    firestore.collection("pondit_messages").add({
      "senderemail": loggedInUser!.email,
      "sendername": loggedInUser!.displayName,
      "text": textEditingController!.text,
      "timestamp": DateTime.now().millisecondsSinceEpoch,
      "imageUrl": imageUrl,
    });
  }

  void sendImageFromGallery() async {
    uploadPhoto(ImageSource.gallery);
  }

  void sendImageFromCamera() async {
    uploadPhoto(ImageSource.camera);
  }

  void uploadPhoto(ImageSource source) async {
    final picker = ImagePicker();
    final file = await picker.pickImage(source: source);

    String fileName =
        "pondit_" + DateTime.now().millisecondsSinceEpoch.toString() + ".jpg";

    Reference reference =
        FirebaseStorage.instance.ref().child("ponditphoto").child(fileName);
    uploadFileInFirebaseStorage(file!, reference);
  }

  void uploadFileInFirebaseStorage(XFile file, Reference reference) async {
    File image = File(file.path);
    await reference.putFile(image);
    String url = await reference.getDownloadURL();
    setState(() {
      saveMessage(url);
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: null,
        actions: <Widget>[
          IconButton(
              icon: const Icon(Icons.close),
              onPressed: () {
                //Implement logout functionality
              }),
        ],
        title: const Text('Chat'),
        backgroundColor: Colors.lightBlueAccent,
      ),
      body: SafeArea(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            MessageStream(),
            Container(
              decoration: kMessageContainerDecoration,
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                children: <Widget>[
                  SizedBox(
                    width: 32,
                    child: IconButton(
                      icon: const Icon(
                        Icons.image,
                        color: Colors.blue,
                        size: 30,
                      ),
                      onPressed: () {
                        sendImageFromGallery();
                      },
                    ),
                  ),
                  SizedBox(
                    width: 32,
                    child: IconButton(
                      icon: const Icon(
                        Icons.camera_alt,
                        color: Colors.blue,
                        size: 30,
                      ),
                      onPressed: () {
                        sendImageFromCamera();
                      },
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: TextField(
                      controller: textEditingController,
                      decoration: kMessageTextFieldDecoration,
                    ),
                  ),
                  TextButton(
                    onPressed: () {
                      saveMessage("empty");
                      textEditingController!.clear();
                    },
                    child: const Text(
                      'Send',
                      style: kSendButtonTextStyle,
                    ),
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class MessageStream extends StatelessWidget {
  const MessageStream({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return StreamBuilder<QuerySnapshot>(
      stream: firestore
          .collection("pondit_messages")
          .orderBy('timestamp', descending: true)
          .snapshots(),
      builder: (_, snapshot) {
        if (snapshot.hasData) {
          List<MessageBubble> messageWidgests = [];
          for (var message in snapshot.data!.docs) {
            final myMessage = message.get('text');
            final mySender = message.get('senderemail');
            final mySenderName = message.get('sendername');
            final myImageUrl = message.get('imageUrl');

            final msgBubble = MessageBubble(
              msgText: myMessage,
              msgSender: mySenderName,
              imageUrl: myImageUrl,
              user: mySender == loggedInUser!.email ? true : false,
            );

            messageWidgests.add(msgBubble);
          }
          return Expanded(
            child: ListView(
              reverse: true,
              children: messageWidgests,
              padding: EdgeInsets.symmetric(horizontal: 10, vertical: 20),
            ),
          );
        } else {
          return Container();
        }
      },
    );
  }
}

class MessageBubble extends StatelessWidget {
  final String? msgText;
  final String? msgSender;
  final bool? user;
  final String? imageUrl;

  MessageBubble({this.msgText, this.msgSender, this.user, this.imageUrl});

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: EdgeInsets.all(10),
      child: Column(
        crossAxisAlignment:
            user == true ? CrossAxisAlignment.end : CrossAxisAlignment.start,
        children: [
          Text(
            msgSender!,
            style: TextStyle(fontSize: 12, color: Colors.white),
          ),
          imageUrl == "empty"
              ? Material(
                  color: user == true ? Colors.blue : Colors.white,
                  borderRadius: BorderRadius.only(
                    bottomLeft: Radius.circular(50),
                    topLeft:
                        user == true ? Radius.circular(50) : Radius.circular(0),
                    bottomRight: Radius.circular(50),
                    topRight:
                        user == true ? Radius.circular(0) : Radius.circular(50),
                  ),
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                        vertical: 10, horizontal: 20.0),
                    child: Text(
                      msgText!,
                      style: TextStyle(
                          fontSize: 15,
                          color: user == true ? Colors.white : Colors.blue),
                    ),
                  ),
                )
              : Container(
                  height: 200,
                  width: 200,
                  alignment: Alignment.center,
                  decoration: BoxDecoration(
                    color: Colors.grey[200],
                    borderRadius: BorderRadius.circular(16),
                  ),
                  child: Image.network(
                    imageUrl!,
                    fit: BoxFit.cover,
                    height: double.infinity,
                    width: double.infinity,
                    alignment: Alignment.center,
                  ),
                )
        ],
      ),
    );
  }
}
