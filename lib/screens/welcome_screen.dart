import 'package:animated_text_kit/animated_text_kit.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:messenger/local_notification/local_notification_service.dart';
import 'package:messenger/screens/login_screen.dart';
import 'package:messenger/screens/registration_screen.dart';

class WelcomeScreen extends StatefulWidget {
  const WelcomeScreen({Key? key}) : super(key: key);

  @override
  _WelcomeScreenState createState() => _WelcomeScreenState();
}

class _WelcomeScreenState extends State<WelcomeScreen>
    with SingleTickerProviderStateMixin {
  AnimationController? controller;
  Animation? animation;

  @override
  void initState() {
    super.initState();
    LocalNotificationService.initialize(context);
    controller =
        AnimationController(vsync: this, duration: Duration(seconds: 3));

    animation = ColorTween(begin: Colors.blueGrey, end: Colors.white)
        .animate(controller!);

    controller!.forward();

    controller!.addListener(() {
      setState(() {});
      print(animation!.value);
    });

    //firebase messaging initialize
    FirebaseMessaging.instance.getInitialMessage().then((mNotification) {
      final name = mNotification!.data["name"];

      print(name);
      if (name == "rakib") {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return LoginScreen(
            message: name,
          );
        }));
      } else if (name == "fahim") {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return RegistrationScreen();
        }));
      } else {
        print(name);
      }
    });

    // get notification data
    FirebaseMessaging.onMessage.listen((mNotification) {
      print(mNotification.notification!.title);
      print(mNotification.notification!.body);

      LocalNotificationService.display(mNotification);
    });

    // background onTap
    FirebaseMessaging.onMessageOpenedApp.listen((mNotification) {
      final name = mNotification.data["name"];

      if (name == "rakib") {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return LoginScreen(
            message: name,
          );
        }));
      } else if (name == "fahim") {
        Navigator.push(context, MaterialPageRoute(builder: (context) {
          return RegistrationScreen();
        }));
      } else {
        print(name);
      }
    });
  }

  @override
  void dispose() {
    super.dispose();
    controller!.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: animation!.value,
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 24.0),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          children: <Widget>[
            Row(
              children: <Widget>[
                Hero(
                  tag: "logoTag",
                  child: SizedBox(
                    child: Image.asset('assets/images/logo.png'),
                    height: 60.0,
                  ),
                ),
                AnimatedTextKit(
                  animatedTexts: [
                    TyperAnimatedText(
                      'Messenger',
                      textStyle: TextStyle(
                        fontSize: 45.0,
                        fontWeight: FontWeight.w900,
                      ),
                    ),
                  ],
                  onTap: () {
                    print("Tap Event");
                  },
                  repeatForever: false,
                ),

                /*Text(
                  'Messenger',
                  style: TextStyle(
                    fontSize: 45.0,
                    fontWeight: FontWeight.w900,
                  ),
                ),*/
              ],
            ),
            const SizedBox(
              height: 48.0,
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Material(
                elevation: 5.0,
                color: Colors.lightBlueAccent,
                borderRadius: BorderRadius.circular(30.0),
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return LoginScreen();
                    }));
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: const Text(
                    'Log In',
                  ),
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.symmetric(vertical: 16.0),
              child: Material(
                color: Colors.blueAccent,
                borderRadius: BorderRadius.circular(30.0),
                elevation: 5.0,
                child: MaterialButton(
                  onPressed: () {
                    Navigator.push(context,
                        MaterialPageRoute(builder: (context) {
                      return RegistrationScreen();
                    }));
                  },
                  minWidth: 200.0,
                  height: 42.0,
                  child: const Text(
                    'Register',
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }
}
